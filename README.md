# docker-compose-cluster-analytics

This docker-compose configuration configures a Prometheus server and
Blackbox server. For external access, configure a separate Traefik container.

Use
[ansible-role-docker-compose](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose)
to start and manage your docker-compose application using Ansible.

With these enviroment variables you can configure Prometheus retention. See the
[Prometheus
docs](https://prometheus.io/docs/prometheus/latest/storage/#operational-aspects)
for more info about these settings.

```sh
PROMETHEUS_RETENTION_TIME=15d
PROMETHEUS_RETENTION_SIZE=0
```
